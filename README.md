# TP.Hui.admin
使用TP5.0 + Hui.admin整合的一套带auth权限管理的基础后台框架。
#特性
>1、自动生成模块代码（控制器、添加、修改、列表、模型、数据表）  

>2、集成第三方类库（微信支付、支付宝支付、oos、七牛）等  

>3、省去重复造轮子的步骤  

>4、开发新项目的效率大大的提高  

>5、方便扩展
# TP.Hui.admin安装
> 1、导入数据库（放在项目的跟目录下）  

> 2、修改数据库配置文件，application下database.php  

> 3、配置Apache或者Nginx（项目跟地址在public下）  
###  apache的config配置示例如下注意【documentRoot】是public下
    <VirtualHost *:80>  
         DocumentRoot "E:\www\Tp.admin\public" 
         ServerName tp.admin.com  
         ServerAlias a.com  
         <Directory "E:\www\Tp.admin\public">  
           Options FollowSymLinks ExecCGI  
           AllowOverride All  
           Order allow,deny  
           Allow from all  
           Require all granted
        </Directory>  
    </VirtualHost>  
#目录结构
~~~
Tp.Hui.Admin  WEB部署目录（或者子目录）
├─application           应用目录
│  ├─admin             后台目录
│  │  ├─controller      后台控制器
│  │  ├─view.php        后台视图
│  ├─extra             扩展目录
│  │  ├─auth.php        auth权限验证类
│  │  ├─General.php     主要封装通用的增加、修改方法
│  │  ├─Generate.php    主要创建一个模块所有的文件
│  │  ├─Sql.php         数据库操作类
│  ├─index             前台目录
│  │  ├─controller      前台控制器
│  ├─model             模型目录
│  ├─temp             模板文件用于文件生成
│  ├─command.php      命令行工具配置文件
│  ├─common.php       公共函数文件
│  ├─config.php       公共配置文件
│  ├─route.php        路由配置文件
│  ├─tags.php         应用行为扩展定义文件
│  └─database.php     数据库配置文件
│
├─public                WEB目录（对外访问目录）
│  ├─static           静态资源目录
│  │  ├─admin             后台静态资源目录，如css,js,images文件
│  ├─index.php          入口文件
│  ├─router.php         快速测试文件
│  └─.htaccess          用于apache的重写
│
├─thinkphp              框架系统目录
│  ├─lang               语言文件目录
│  ├─library            框架类库目录
│  │  ├─think           Think类库包目录
│  │  └─traits          系统Trait目录
│  │
│  ├─tpl                系统模板目录
│  ├─base.php           基础定义文件
│  ├─console.php        控制台入口文件
│  ├─convention.php     框架惯例配置文件
│  ├─helper.php         助手函数文件
│  ├─phpunit.xml        phpunit配置文件
│  └─start.php          框架入口文件
│
├─extend                扩展类库目录
├─runtime               应用的运行时目录（可写，可定制）
├─vendor                第三方类库目录（Composer依赖库）
├─build.php             自动生成定义文件（参考）
├─composer.json         composer 定义文件
├─LICENSE.txt           授权说明文件
├─README.md             README 文件
├─think                 命令行入口文件
~~~
说明：非常适合新项目的开发和扩展，目前还是以传统的curd模式操作，封装太深看起来就太复杂了，而且不易扩展。  

该系统能够很快速的帮您搭建一个新项目，而且他能够快速的帮你生成其他模块的代码，不需要你重复的造轮子，ctrl+c\ctrl+v的过程。  
  
如果是单表操作你完全可以不用修改任何代码，本系统从数据表、到控制器、模型、视图（增加、修改、列表、搜索）都一条龙自动生成。

让你花一分钟的时间就可以完成一个大的模块从建表到显示整个过程，如果你原来写一个curd模块需要花2个小时，你现在只需要1分钟。  

刚上线必定有很多没有考虑到的地方，必定会有一些bug,那么你可以和我一起来修复它、完善它！和我一起你可以更加快速的掌握它！

也希望小伙伴给我多多提建议和支持！如果你喜欢可以给我个star我会跟家努力！