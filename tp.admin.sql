/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : tp.admin

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2017-08-29 11:20:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tp_admin
-- ----------------------------
DROP TABLE IF EXISTS `tp_admin`;
CREATE TABLE `tp_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(60) NOT NULL DEFAULT '' COMMENT '密码',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态，1启用，2禁用',
  `last_login_time` int(11) NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `last_login_ip` int(11) NOT NULL DEFAULT '0' COMMENT '最后登录ip',
  `login_times` int(11) NOT NULL DEFAULT '0' COMMENT '登录次数',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- ----------------------------
-- Records of tp_admin
-- ----------------------------
INSERT INTO `tp_admin` VALUES ('1', 'admin', '$2y$10$v5/ZS9LqaP724DsOJJgureTChsRnhJXSQOfSfXR0QE4zJ6TzOq4r6', '1', '1492676235', '-1263680484', '293', '1489658790');
INSERT INTO `tp_admin` VALUES ('11', 'jin88251', '$2y$10$p3wNcEwywawYypG3c/UTNe/X8.Opd6SuQgWmH1883kMnUMxesgu2u', '1', '0', '0', '0', '1503453335');

-- ----------------------------
-- Table structure for tp_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `tp_auth_group`;
CREATE TABLE `tp_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1启用，2禁用',
  `rules` varchar(500) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '角色分组注释',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_auth_group
-- ----------------------------
INSERT INTO `tp_auth_group` VALUES ('1', '系统管理员', '1', '1,2,3', '查看');
INSERT INTO `tp_auth_group` VALUES ('2', '栏目管理员', '1', '1,3', '管理栏目动态');
INSERT INTO `tp_auth_group` VALUES ('4', '超级管理', '1', '1,5,6,7,8,9,10,11,25,,93,94,', '直方大法搜佛纳双方');

-- ----------------------------
-- Table structure for tp_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `tp_auth_group_access`;
CREATE TABLE `tp_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_auth_group_access
-- ----------------------------
INSERT INTO `tp_auth_group_access` VALUES ('11', '4');

-- ----------------------------
-- Table structure for tp_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `tp_auth_rule`;
CREATE TABLE `tp_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(80) NOT NULL DEFAULT '' COMMENT '验证规则标识',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '验证规则名称',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态 是否启用',
  `condition` char(100) NOT NULL DEFAULT '',
  `p_id` int(11) NOT NULL DEFAULT '0' COMMENT '上级的ID编号',
  `is_show` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0显示，1不显示',
  `icon` varchar(20) NOT NULL DEFAULT '' COMMENT '图片标记（一般顶级栏目才有）',
  `orderby` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`) USING HASH
) ENGINE=MyISAM AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_auth_rule
-- ----------------------------
INSERT INTO `tp_auth_rule` VALUES ('1', '', '系统管理', '1', '1', '', '0', '0', '#xe63c', '0');
INSERT INTO `tp_auth_rule` VALUES ('2', '', '栏目管理', '1', '1', '', '0', '0', '#xe681', '0');
INSERT INTO `tp_auth_rule` VALUES ('3', 'admin/rule/ruleList', '权限管理', '1', '1', '', '1', '0', '', '0');
INSERT INTO `tp_auth_rule` VALUES ('4', 'admin/rule/cu', '添加、修改权限', '1', '1', '', '1', '1', '', '0');
INSERT INTO `tp_auth_rule` VALUES ('96', 'admin/code/codeTool', '代码生成工具', '1', '1', '', '1', '0', '', '0');
INSERT INTO `tp_auth_rule` VALUES ('6', 'admin/role/roleList', '角色管理', '1', '1', '', '1', '0', '', '0');
INSERT INTO `tp_auth_rule` VALUES ('7', 'admin/group/cu', '添加、修改角色', '1', '1', '', '1', '1', '', '0');
INSERT INTO `tp_auth_rule` VALUES ('9', 'Admin/Admin/adminList', '管理员管理', '1', '1', '', '1', '0', '', '0');
INSERT INTO `tp_auth_rule` VALUES ('10', 'Admin/Admin/cu', '添加、修改管理员', '1', '1', '', '1', '1', '', '0');
INSERT INTO `tp_auth_rule` VALUES ('25', 'admin/system/system', '系统设置', '1', '1', '', '1', '0', '', '0');

-- ----------------------------
-- Table structure for tp_config
-- ----------------------------
DROP TABLE IF EXISTS `tp_config`;
CREATE TABLE `tp_config` (
  `cname` varchar(50) NOT NULL DEFAULT '' COMMENT '配置名称',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '显示名称',
  `option` text COMMENT '配置内容',
  PRIMARY KEY (`cname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='配置表';

-- ----------------------------
-- Records of tp_config
-- ----------------------------
INSERT INTO `tp_config` VALUES ('allow_ip', '允许访问后台的IP列表', '1');
INSERT INTO `tp_config` VALUES ('case_number', '备案号', ' 京ICP备15067335号-4 ');
INSERT INTO `tp_config` VALUES ('copyright_information', '底部版权信息', 'Copyright © 2017 qifanfuwu.com Inc. All Rights Reserved. 企凡网 版权所有');
INSERT INTO `tp_config` VALUES ('count_code', '统计代码', '&lt;script src=&quot;https://s11.cnzz.com/z_stat.php?id=1261856949&amp;web_id=1261856949&quot; language=&quot;JavaScript&quot;&gt;&lt;/script&gt;');
INSERT INTO `tp_config` VALUES ('description', '描述', '碧有信.');
INSERT INTO `tp_config` VALUES ('failure_number', '访问后台允许的失败次数', '1');
INSERT INTO `tp_config` VALUES ('site_keyword', '网站关键词', '碧有信');
INSERT INTO `tp_config` VALUES ('site_name', '网站名称', 'TP5.0.admin');
INSERT INTO `tp_config` VALUES ('site_url', '网站地址', 'http://biyouxin.com');
INSERT INTO `tp_config` VALUES ('site_version', '网站版本', 'v1.0');

-- ----------------------------
-- Table structure for tp_form_html
-- ----------------------------
DROP TABLE IF EXISTS `tp_form_html`;
CREATE TABLE `tp_form_html` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) NOT NULL,
  `table_form_html` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_form_html
-- ----------------------------
