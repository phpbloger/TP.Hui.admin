<?php
// +----------------------------------------------------------------------
// | Created by [ PhpStorm ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 上海到啦网络科技有限公司.
// +----------------------------------------------------------------------
// | Create Time ( 2017/8/21 0021 - 下午 2:28)
// +----------------------------------------------------------------------
// | Author: tangyijun <251784425@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;
use app\extra\General;
use app\extra\Sql;
/**
* Class Rule
* @package app\admin\controller
* 权限规则
*/
class -controller_name- extends Base {
    private $table_name = '-table_name-';
    public function -controller_name-List(){
        $param = -search-;
        $search = General::search($param);
        $list = Sql::_select($this->table_name,$search['where']);
        return view('-controller_name-List',[
        'list' => $list,
        'search' => $search['search']
        ]);
    }

    /**
    * @return array|\think\response\Json|\think\response\View
    * 增加和修改的方法
    */
    public function cu(){
        if(request()->isPost()){
            $res = General::generalAdd($this->table_name,false);
            if($res){
                return formatSuccessResult();
            }else{
                return formatResult($res['errorMsg'],10020);
            }
        }
        $id = intval(input('param.id'));
        $one = Sql::_find($this->table_name,['id' => $id]);
        $result = [
            'one' => $one,
        ];
        return view('cu',['result' => $result]);
        }
    }