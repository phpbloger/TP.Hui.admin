{extend name="public/base" /}
{block name="main"}
    <nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 管理员管理 <span class="c-gray en">&gt;</span> 管理员列表 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
    <div class="page-container">
        {include file="/-list_action-/search" /}
        <div class="cl pd-5 bg-1 bk-gray mt-20">
            -more_del_button-
            <span class="l">
                <a href="javascript:;" onclick="admin_add('添加记录','{:url('-list_action-/cu')}','800','500')" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe600;</i> 添加记录</a>
            </span>
        </div>

        <table class="table table-border table-bordered table-bg">
            <thead>
            <tr>
                <th scope="col" colspan="7">列表</th>
            </tr>
            <tr class="text-c">
                -more_del_head-
                -list_head-
                <th width="70">操作</th>
            </tr>
            </thead>

            <tbody>
            <?php foreach($list as $v){?>
            <tr class="text-c">
                -list_more_del-
                -list_td-
                <td>
                    -list_button-
                </td>
            </tr>
            <?php } ?>
            </tbody>
        </table>

    </div>
{/block}
{block name="js"}
    <script type="text/javascript">
        /*
         参数解释：
         title	标题
         url		请求的url
         id		需要操作的数据id
         w		弹出层宽度（缺省调默认值）
         h		弹出层高度（缺省调默认值）
         */
        function admin_add(title,url,w,h){
            layer_show(title,url,w,h);
        }
        /*管理员-编辑*/
        function rule_edit(title,url,id,w,h){
            layer_show(title,url,w,h);
        }

        function del(table_name,id){
            layer.confirm('确认要删除吗？',function(index){
                var where = "id = "+id;
                $.ajax({
                    type:'post',
                    url:"<?php echo url('universal/del')?>",
                    data:{"table_name":table_name,"where":where},
                    success:function(data){
                        if(typeof data === 'string'){
                            data = jQuery.parseJSON(data);
                        }
                        if(data.code ==10000){
                            layer.msg(data.msg,{icon:1,time:1000});
                            setTimeout("loadPage()",1200)
                        }else{
                            layer.msg(data.msg,{icon:5,time:1000});
                        }
                    }
                });
            });
        }

        function datadel(table_name){
            layer.confirm('确认要批量删除吗？',function(index){
                var chk_value = '';
                $('input[name="ids"]:checked').each(function(){
                    chk_value += $(this).val() + ',';
                });
                if(chk_value.length == 0){
                    layer.msg('你还没有选择任何内容',{icon: 1,time:1000});
                }
                $.ajax({
                    type:'post',
                    url:"<?php echo url('universal/delAll')?>",
                    data:{'ids':chk_value,'table_name':table_name},
                    success:function (data) {
                        if(typeof data === 'string'){
                            data = jQuery.parseJSON(data);
                        }
                        if(data.code == 10000){
                            layer.msg(data.msg,{icon: 1,time:1000});
                            setTimeout("loadPage()",1200)
                        }else{
                            layer.msg(data.msg,{icon:5,time:1000});
                        }
                    }
                });

            });

        }
        function loadPage() {
            location.reload();
        }
    </script>
{/block}