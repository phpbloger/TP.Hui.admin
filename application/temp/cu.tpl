{extend name="public/base" /}
{block name="main"}
    <article class="page-container">
        <form action="" method="post" class="form form-horizontal" id="form-admin-role-save" >
            -form_html-
            <div class="row cl">
                <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
                    <input type="hidden" value="<?php echo $one['id']?>" name="id">
                    <button type="submit" class="btn btn-success radius" id="admin-role-save"><i class="icon-ok"></i> 确定</button>
                </div>
            </div>
        </form>
    </article>
{/block}
{block name="js"}
    <script type="text/javascript">
        $(function(){
            $("#form-admin-role-save").validate({
                rules:{
                    title:{
                        required:true,
                    },
                },
                submitHandler:function(form){
                    /**
                     * 成功之后的回调函数
                     * @type {{success: options.success}}
                     */
                    var options = {
                        success: function (data) {
                            if(data.code==10000){
                                layer.msg(data.msg,{icon:1,time:1000});
                                setInterval("closeWindow()",1000);
                            }else{
                                layer.msg(data.msg,{icon:5,time:1000});
                            }
                        }
                    };
                    $(form).ajaxSubmit(options);
                }
            });
        });

        function closeWindow(){
            window.parent.location.reload();
        }
    </script>
{/block}