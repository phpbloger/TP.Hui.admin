<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------
const UPLOAD_THUMB_OFF = false;  //是否生成缩略图
const UPLOAD_THUMB_ON  = true;
// 应用公共文件
/**
 * @param null $data
 * @return \think\response\Json
 * 操作成功
 */
function formatSuccessResult($data = null){
    return json(['code' => 10000,'msg' => '操作成功','data' => $data]);
}
/**
 * @param int $code
 * @param string $errorMsg
 * @param string $data
 * @return array
 * 失败信息
 */
function formatResult($errorMsg,$code, $data = null){
    return json(['code' => $code,'msg' => $errorMsg,'data'=>$data]);
}

/**
 * @return bool|mixed
 * 后台管理员登陆状态
 */
function admin_is_login(){
    $admin_id = \think\Request::instance()->cookie('admin_admin_id');
    if(!$admin_id){
        return false;
    }
    return $admin_id;
}

/**
 * @param array $list
 * @param int $parent_id
 * @param int $deep
 * @return array
 * 无限极分类
 */
function infinite($list = [],$parent_id = 0,$deep = 0){
    static $arr = [];
    foreach ($list as $v){
        if($v['p_id'] == $parent_id){
            $v['deep'] = $deep;
            $arr[] = $v;
            infinite($list,$v['id'],$deep + 1);
        }
    }
    return $arr;
}

/**
 * @param bool $uploads_thumb
 * @param int $width
 * @param int $height
 * @return array
 * 图片上传
 */
function upload($uploads_thumb = UPLOAD_THUMB_OFF,$width = 0,$height = 0){
    $file = request()->file("image");
    if(!empty($_FILES) && empty($file)){
        return ['errorMsg'=>'上传文件大小超过配置'];
    }
    $info = $file->validate(['size'=>15567998,'ext'=>'jpg,png,gif,jpeg'])->move(ROOT_PATH . 'public' . DS . 'uploads');
    if(!$info){
        return ['errorMsg'=>$file->getError()];
    }
    $file_path = $info->getSaveName();
    //需要生成缩略图
    if($uploads_thumb === UPLOAD_THUMB_ON){
        $file_name =  $info->getFilename();
        $thumb = \think\Image::open(ROOT_PATH . 'public' . DS . 'uploads'.DS.$file_path);
        $thumb->thumb($width,$height,\think\Image::THUMB_CENTER)->save(ROOT_PATH . 'public' . DS . 'uploads'.DS.date('Ymd').DS.$file_name.'thumb.jpg');
    }
    return [
        'file_path'=>$file_path,
        'thumb_path' => date('Ymd').DS.$file_name.'thumb.jpg'
    ];
}

/**
 * @param $fileName
 * @return array
 * 多图上传
 */
function uploadMore($fileName){
    // Get the form upload file
    $files = request()->file('image');
    foreach($files as $file){
        // Move to the framework application root directory / public / uploads / directory
        $info = $file->validate(['size'=>20971520,'ext'=>'jpg,png,gif'])->rule('uniqid')->move(ROOT_PATH . 'public' . DS . 'uploads' . DS . $fileName);
        if(!$info){
            // Upload failed to get error message
            return ['errorMsg'=>$file->getError()];
        } else {
            // Upload upload information after successful upload
            $filePath[]   = $fileName. '/' .$info->getSaveName();
        }
    }
    return $filePath;
}

function showMsg($msg){
    echo "<br> ".$msg."......";
    echo str_pad('',4096)."\n";
    ob_flush();
    flush();
    sleep(2);
    ob_end_flush();
}

function jsonString($str){
    return preg_replace("/([\\\\\/'])/",'\\\$1',$str);
}

function getConfig(){
    $config = [];
    $result = \app\extra\Sql::_select('config');
    if(!empty($result)){
        foreach($result as $v){
            $config[$v['cname']] = $v['option'];
        }
    }
    return $config;
}
