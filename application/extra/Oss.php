<?php
// +----------------------------------------------------------------------
// | Created by [ PhpStorm ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 上海到啦网络科技有限公司.
// +----------------------------------------------------------------------
// | Create Time ( 2017/8/30 0030 - 下午 2:29)
// +----------------------------------------------------------------------
// | Author: tangyijun <251784425@qq.com>
// +----------------------------------------------------------------------
namespace app\extra;
use OSS\Core\OssException;
use OSS\OssClient;
class Oss{
    private static $instance;
    private $oss_config;
    private $put_object  = 'object';
    private $upload_file = 'file';
    private function __construct(){
        $this->oss_config = getConfig();
    }
    private function __clone(){
        // TODO: Implement __clone() method.
    }

    public static function getInstance(){
        if(!self::$instance instanceof  Oss){
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @return array|OssClient
     * 创建oss对象
     */
    public function clientOss(){
        if(empty($this->oss_config['access_key_id']) || !isset($this->oss_config['access_key_id'])){
            return ['errorMsg' => '请在后台设置阿里云access key id'];
        }
        if(empty($this->oss_config['access_key_secret']) || !isset($this->oss_config['access_key_secret'])){
            return ['errorMsg' => '请在后台设置阿里云access key secret'];
        }
        if(empty($this->oss_config['endpoint']) || !isset($this->oss_config['endpoint'])){
            return ['errorMsg' => '请在后台设置阿里云endpoint'];
        }
        try{
            $oss_client = new OssClient($this->oss_config['access_key_id'],$this->oss_config['access_key_secret'],$this->oss_config['endpoint']);
        }catch (OssException $e){
            return ['errorMsg' => $e->getMessage()];
        }
        return $oss_client;
    }

    /**
     * @param $oss_path    oss保存的文件路径（如果没有将会自动创建）
     * @param $file_path   本地文件的保存路径
     * @param $action      上传类型
     * @return array|bool
     */
    function ossUpload($oss_path,$file_path,$action){
        $oss_client = $this->clientOss(); //链接
        switch ($action){
            case $this->put_object === $action:
                try{
                    $content = file_get_contents($file_path);
                    $oss_client->putObject($this->oss_config['bucket'],$oss_path,$content);
                } catch(OssException $e) {
                    return ['errorMsg' => $e->getMessage()];
                }
                return true;
                break;
            case $this->upload_file === $action:
                try{
                    $oss_client->uploadFile($this->oss_config['bucket'],$oss_path,$file_path);
                }catch (OssException $e){
                    return ['errorMsg' => $e->getMessage()];
                }
                return true;
                break;

            default:
        }

        return true;
    }


}