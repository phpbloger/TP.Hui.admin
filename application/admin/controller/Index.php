<?php
// +----------------------------------------------------------------------
// | Created by [ PhpStorm ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 上海到啦网络科技有限公司.
// +----------------------------------------------------------------------
// | Create Time ( 2017/8/18 0018 - 下午 4:46)
// +----------------------------------------------------------------------
// | Author: tangyijun <251784425@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;
use app\extra\Sql;

class Index{
    public function index(){
        $result = [
            'label' => '首页',
            'menu'  => Menu::getMenu(),
            'config' => getConfig()
        ];
        return view('index',[
            'result' => $result
        ]);
    }

    public function main(){
        $config = [];
        $result = Sql::_select('config');
        if(!empty($result)){
            foreach($result as $v){
                $config[$v['cname']] = $v['option'];
            }
        }
        return view('main',['result'=>getConfig()]);
    }
}