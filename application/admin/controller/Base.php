<?php
// +----------------------------------------------------------------------
// | Created by [ PhpStorm ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 上海到啦网络科技有限公司.
// +----------------------------------------------------------------------
// | Create Time ( 2017/8/21 0021 - 上午 11:42)
// +----------------------------------------------------------------------
// | Author: tangyijun <251784425@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;
use app\extra\Auth;

/**
 * Class Base
 * @package app\admin\controller
 * 基础类，作为权限验证类
 */
class Base{
    public function _initialize(){
        //没有登录直接跳转到登录页
        if(admin_is_login() <= 0){
            $this->redirect('login/login');
        }
        if(admin_is_login() == 1){
            return true;
        }
        /*验证过滤 以下页面不做验证*/
        if(request()->action() == 'getSearch' || request()->action() == 'del' || request()->action() == 'index' || request()->action() == 'logout' || request()->action() == 'login' || request()->action() ==  'main'){
            return true;
        }
        $auth = new Auth();
        if ($auth->check(Request::instance()->module() . '/' . Request::instance()->controller() . '/' . Request::instance()->action(), is_login())) {
            return true;
        } else {
            if(request()->isAjax()){
                exit(json_encode(['code'=>'100020','msg'=>'抱歉，您不具备该权限!']));
            }
$html=<<<html
    <html>
    <head>
        <link rel="stylesheet" type="text/css" href="/static/admin/css/H-ui.admin.css" />
        <link href="/static/admin/static/h-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="/static/admin/lib/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript" src="/static/admin/lib/layer/2.4/layer.js"></script>
    </head>
    <body>
        <script type="text/javascript">
            $(function(){
              setInterval("closeWindow()",1500);
                layer.msg('抱歉，您不具备该权限！',{icon: 7,time:1500});
              });
            function closeWindow(){
                var index = parent.layer.getFrameIndex(window.name);
                parent.layer.close(index);
           }
        </script>
    </body>
</html>
html;
            echo $html;exit;
        }
    }
}