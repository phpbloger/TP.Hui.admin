<?php
// +----------------------------------------------------------------------
// | Created by [ PhpStorm ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 上海到啦网络科技有限公司.
// +----------------------------------------------------------------------
// | Create Time ( 2017/8/22 0022 - 下午 4:15)
// +----------------------------------------------------------------------
// | Author: tangyijun <251784425@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;
use app\extra\General;
use app\extra\Sql;

/**
 * Class Role
 * @package app\admin\controller
 * 角色类
 */
class Role extends Base {
    private $table_name = 'auth_group';
    /**
     * @return \think\response\View
     * 角色列表
     */
    public function roleList(){
        $param = [
            'title'=>'like'
        ];
        $search = General::search($param);
        $list = Sql::_select($this->table_name,$search['where']);
        return view('roleList',[
            'list' => $list,
            'search' => $search['search']
        ]);
    }

    /**
     * @return array|\think\response\Json|\think\response\View
     * 角色添加修改操作
     */
    public function cu(){
        if(request()->isPost()){
            $post = request()->post();
            $post['rules'] = implode(',',$post['ids']);
            unset($post['ids']);
            if(empty($post['id'])){
                $res = \app\model\Role::_add($post);
            }else{
                $res = \app\model\Role::_update($post);
            }
            if($res){
                return formatSuccessResult();
            }else{
                return formatResult('数据库操作失败',10020);
            }
        }
        $id = intval(input('id'));
        $one = Sql::_find($this->table_name,['id' => $id]);
        $list = Sql::_select('auth_rule',['p_id' => 0]);
        foreach($list as $v){
            $result[$v['title']] = Sql::_select('auth_rule',['p_id' => $v['id']]);
        }
        return view('cu',[
            'list'=>$result,
            'one'=>$one
        ]);
    }
}