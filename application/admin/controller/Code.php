<?php
// +----------------------------------------------------------------------
// | Created by [ PhpStorm ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 上海到啦网络科技有限公司.
// +----------------------------------------------------------------------
// | Create Time ( 2017/8/24 0024 - 下午 4:56)
// +----------------------------------------------------------------------
// | Author: tangyijun <251784425@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;
use app\extra\Generate;
use app\extra\Sql;

class Code{
    public function codeTool(){
        return view('codeTool');
    }
    /**
     * 开始生成文件
     */
    public function generate(){
        $res = Generate::run(); //调用代码生成程序
        if($res){
            echo '<br><a href="'.$res.'">点击进入下一步</a>';
        }else{
            echo '<a>点击进入上一步</a>';
        }
    }

    /**
     * @return array|\think\response\Json|\think\response\View
     * 创建修改视图
     */
    public function form(){
        $get = input('param.');
        $table_name = \think\Config::get('database.prefix').$get['table_name'];
        $sql = "desc {$table_name}";
        $file_desc = Sql::_query($sql);
        foreach($file_desc as $v){
            if($v['Field'] == 'title'){
                $path = APP_PATH.'temp'.DS;
                $path = $path.'input.tpl';
                $temp = file_get_contents($path);
                $temp =  str_replace("-field_comment-", '标题', $temp);
                //替换字段
                $temp =  str_replace("-field_name-", 'title', $temp);
            }
        }
        //查询改字段是否存在
        $one = Sql::_find('form_html',['table_name' => $table_name,'table_form_html' => $temp]);
        if(empty($one)){
            Sql::_save('form_html',['table_name' => $table_name,'table_form_html' => $temp]);
        }
        //读取添加字段的
        $field_form_html = Sql::_select('form_html',['table_name' => $table_name]);
        if(request()->isPost()){
            $res = Generate::createCuView();
            $res1 = Generate::createListView();
            if($res && $res1){
                return formatSuccessResult();
            }else{
                return formatResult($res['errorMsg'],10020);
            }
        }
        return view('form',[
            'file_desc'=> $file_desc,
            'get'=>$get,
            'field_form_html' => $field_form_html,
        ]);
    }

    /**
     * @return array|\think\response\Json|\think\response\View
     * 添加字段
     */
    public function addField(){
        $get = input('param.');
        $table_name = \think\Config::get('database.prefix').$get['table_name'];
        if(request()->isPost()){
            $res = Generate::createField();
            if($res === true){
                return formatSuccessResult();
            }else{
                return formatResult($res['errorMsg'],10020);
            }
        }
        return view('addField',['table_name'=>$table_name,'get'=>$get]);
    }


}