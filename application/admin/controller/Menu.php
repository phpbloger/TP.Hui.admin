<?php
// +----------------------------------------------------------------------
// | Created by [ PhpStorm ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 上海到啦网络科技有限公司.
// +----------------------------------------------------------------------
// | Create Time ( 2017/8/21 0021 - 上午 11:50)
// +----------------------------------------------------------------------
// | Author: tangyijun <251784425@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;
use app\extra\Sql;

/**
 * Class Menu
 * @package app\admin\controller
 * 获取菜单栏
 */
class Menu{
    const ADMIN_TABLE_NAME = 'admin';
    const AUTH_RULE_TABLE_NAME = 'auth_rule';
    public static function getMenu(){
        $aid = empty(admin_is_login()) ? 0 : admin_is_login(); //获取当前用户登陆的id
        $rule_ids = Sql::_joinOne(
            self::ADMIN_TABLE_NAME,
            [['auth_group_access b','a.id = b.uid','left'],['auth_group c','b.group_id = c.id','left']],
            ['a.id' => $aid],
            'a.*,c.rules'
        );
        $rule_ids = empty($rule_ids) ? 0 : $rule_ids;
        if(empty($rule_ids)){
            exit('你不具备任何管理权限无法登录');
        }
        //如果是超级管理员则获取所有的权限
        if($aid == Sql::ROLE_ADMIN){
            $parent_menu_where = "p_id = 0";
            $son_menu_where = "p_id != 0 and is_show = 0";

        }else{
            $parent_menu_where = "id in ({$rule_ids['rules']}) and p_id = 0";
            $son_menu_where  = "id in ({$rule_ids['rules']}) and p_id != 0 and is_show = 0";
        }

        $parent_menu = Sql::_select(
            self::AUTH_RULE_TABLE_NAME,
            $parent_menu_where,
            [],
            '*',
            'orderby asc'
        );
        $son_menu    = Sql::_select(
            self::AUTH_RULE_TABLE_NAME,
            $son_menu_where,
            [],
            '*',
            'orderby asc'
        );
        $menu = [];
        foreach($parent_menu as $k=>$v){
            foreach($son_menu as $k1=>$v1){
                if($v1['p_id'] == $v['id']){
                    $menu[$k]['parentMenu'] = $v;
                    $menu[$k]['sonMenu'][]  = $v1;
                }
            }
        }
        return $menu;
    }
}