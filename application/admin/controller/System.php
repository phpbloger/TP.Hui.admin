<?php
// +----------------------------------------------------------------------
// | Created by [ PhpStorm ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 上海到啦网络科技有限公司.
// +----------------------------------------------------------------------
// | Create Time ( 2017/8/23 0023 - 上午 11:14)
// +----------------------------------------------------------------------
// | Author: tangyijun <251784425@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;
use app\extra\Sql;
use think\Request;

class System extends Base{
    private $table_name = 'config';
    public function system(){
        $config = [];
        $result = Sql::_select($this->table_name);
        if(!empty($result)){
            foreach($result as $v){
                $config[$v['cname']] = $v['option'];
            }
        }
        if(Request::instance()->isPost()){
            $post = input('param.');
            foreach( $post as $k=>$v){
                if( $v != $config[$k]){
                    Sql::_setField($this->table_name,['cname' => $k],['option' =>  $v]);
                    $config[$k] = $v;
                }
            }
            return formatSuccessResult();
        }
        return view('system',['result'=>$config]);
    }
}