<?php
// +----------------------------------------------------------------------
// | Created by [ PhpStorm ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 上海到啦网络科技有限公司.
// +----------------------------------------------------------------------
// | Create Time ( 2017/8/22 0022 - 下午 4:57)
// +----------------------------------------------------------------------
// | Author: tangyijun <251784425@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;
use app\extra\General;
use app\extra\Sql;

/**
 * Class Admin
 * @package app\admin\controller
 * 管理员操作类
 */
class Admin extends Base{
    private $table_name = 'admin';
    /**
     * @return \think\response\View
     * 系统管理员列表
     */
    public function adminList(){
        $param = [
            'username'=>'like'
        ];
        $search = General::search($param,'a.id > 0');
        $list = Sql::_select($this->table_name,$search['where'],[['auth_group_access b','a.id=b.uid','left'],['auth_group c','b.group_id = c.id','left']],'a.*,c.title');
        return view('adminList',[
            'list' => $list,
            'search' => $search['search']
        ]);
    }

    /**
     * @return array|\think\response\Json|\think\response\View
     * 系统管理员修改和添加
     */
    public function cu(){
        if(request()->isPost()){
            $res = \app\model\Admin::_add();
            if($res){
                return formatSuccessResult();
            }else{
                return formatResult($res['errorMsg'],10020);
            }
        }
        $result = Sql::_select('auth_group');
        $id = intval(input('id'));
        $one = Sql::_joinOne($this->table_name,[['auth_group_access b','a.id = b.uid','left']],['a.id'=> $id],'a.*,b.*');
        return view('cu',[
            'result' => $result,
            'one' => $one
        ]);
    }

    /**
     * @return \think\response\View
     * 重置管理员密码
     */
    public function resetPwd(){
        if(request()->isPost()){
            $res = \app\model\Admin::password();
            if($res){
                return formatSuccessResult();
            }else{
                return formatResult($res['errorMsg'],10020);
            }
        }
        $id = intval(input('param.id'));
        $one = Sql::_find($this->table_name,['id' => $id]);
        return view('resetPwd',[
            'one' => $one
        ]);
    }
}