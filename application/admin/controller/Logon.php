<?php
// +----------------------------------------------------------------------
// | Created by [ PhpStorm ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 上海到啦网络科技有限公司.
// +----------------------------------------------------------------------
// | Create Time ( 2017/8/18 0018 - 下午 5:54)
// +----------------------------------------------------------------------
// | Author: tangyijun <251784425@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;
class Logon{
    public function login(){
        if(request()->isPost()){
            $res = \app\model\Logon::_login();
            if($res!==true){
               return formatResult($res['errorMsg'],10020);
            }
            return formatSuccessResult();
        }
        return view('login');
    }

}