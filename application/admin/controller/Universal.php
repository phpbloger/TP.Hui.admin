<?php
// +----------------------------------------------------------------------
// | Created by [ PhpStorm ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 上海到啦网络科技有限公司.
// +----------------------------------------------------------------------
// | Create Time ( 2017/8/23 0023 - 上午 10:11)
// +----------------------------------------------------------------------
// | Author: tangyijun <251784425@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;
use app\extra\Sql;

/**
 * Class Universal
 * @package app\admin\controller
 * 通用的标识修改类
 */
class Universal extends Base {
    /**
     * @return array|\think\response\Json
     * 通用的删除方法
     */
    public function universalDel(){
        $post = \think\Request::instance()->post();
        $res = Sql::_del($post['table_name'],$post['where']);
        if($res){
            return formatSuccessResult();
        }
        return formatResult('数据库操作失败',10020);
    }

    /**
     * @return mixed
     * 通用的批量删除方法
     */
    public function delAll(){
        $post = \think\Request::instance()->post();
        $ids = trim($post['ids'],',');
        $res = Sql::_del($post['table_name'],"id in({$ids})");
        if($res){
            return formatSuccessResult();
        }
        return formatResult('数据库操作失败',10020);
    }
    /**
     * @return array|\think\response\Json
     * 通用的修改数据库某一个字段的值
     */
    public function changeStatus(){
        $post = \think\Request::instance()->post();
        $res = Sql::_setField($post['table_name'],['id' => $post['id']],[$post['field'] => $post['value']]);
        if($res){
            return formatSuccessResult();
        }
        return formatResult('数据库操作失败',10020);
    }

    /**
     * @return \think\response\Json
     * xheditor 图片上传
     */
    public function _upload(){
        if(!empty($_FILES)){
            $res = upload();
            //{"err":"","msg":"200906030521128703.gif"}
            if(empty($res['errorMsg'])){
                //成功上传
                $result = ['err'=>'','msg' => '/uploads/'.$res['file_path']];
            }else{
                $result = ['err' => $res['errorMsg']];
            }
            return json($result);
        }
    }
}