<?php
// +----------------------------------------------------------------------
// | Created by [ PhpStorm ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 上海到啦网络科技有限公司.
// +----------------------------------------------------------------------
// | Create Time ( 2017/8/21 0021 - 下午 2:28)
// +----------------------------------------------------------------------
// | Author: tangyijun <251784425@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;
use app\extra\General;
use app\extra\Sql;

/**
 * Class Rule
 * @package app\admin\controller
 * 权限规则
 */
class Rule extends Base {
    private $table_name = 'auth_rule';
    public function ruleList(){
        $param = [
            'title'=>'like'
        ];
        $search = General::search($param);
        $list = Sql::_select($this->table_name,$search['where']);
        $list = infinite($list,0,0);
        return view('ruleList',[
            'list' => $list,
            'search' => $search['search']
        ]);
    }
    /**
     * @return array|\think\response\Json|\think\response\View
     * 增加和修改的方法
     */
    public function cu(){
        if(request()->isPost()){
            $res = \app\model\Rule::_cu();
            if($res){
                return formatSuccessResult();
            }else{
                return formatResult($res['errorMsg'],10020);
            }
        }
        //如果是修改返回当前的数据
        $id = intval(input('param.id'));
        $one = Sql::_find($this->table_name,['id' => $id]);
        //读取下拉数据
        $parent_rule = Sql::_select($this->table_name,'p_id = 0',[],'id,title');
        $result = [
            'one' => $one,
            'parent_rule' => $parent_rule
        ];
        return view('cu',['result' => $result]);
    }
}