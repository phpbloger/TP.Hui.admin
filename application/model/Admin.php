<?php
// +----------------------------------------------------------------------
// | Created by [ PhpStorm ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 上海到啦网络科技有限公司.
// +----------------------------------------------------------------------
// | Create Time ( 2017/8/22 0022 - 下午 6:06)
// +----------------------------------------------------------------------
// | Author: tangyijun <251784425@qq.com>
// +----------------------------------------------------------------------
namespace app\model;
use app\extra\Sql;
use think\Db;

class Admin{
    const TABLE_NAME = 'admin';
    const TABLE_NAME_PEX = 'auth_group_access';
    public static function _add(){
        $post = request()->post();
        if(!empty($post['uid'])){
            return self::_save();
        }
        $data['username'] = $post['username'];
        $data['password'] = password_hash($post['password'],true); //密码加密
        $data['create_time'] = time();
        $data['status'] = $post['status'];
        try{
            Db::startTrans();
            $res1 = Sql::_save(self::TABLE_NAME,$data);
            $res2 = Sql::_save(self::TABLE_NAME_PEX,['uid'=>$res1,'group_id'=>$post['group_id']],false);
            if($res1 && $res2){
                Db::commit();
                return true;
            }
        }catch (\Exception $e){
            Db::rollback();
            return ['errorMsg' => '数据库操作失败'];
        }
    }

    public static function _save(){
        $post = request()->post();
        return Sql::_update(self::TABLE_NAME_PEX,['uid'=>$post['uid']],['group_id'=>$post['group_id']]);
    }

    public static function password(){
        $post = request()->post();
        $password = password_hash($post['password'],true);
        return Sql::_update(self::TABLE_NAME,['id'=> $post['id']],['password'=>$password]);
    }
}