<?php
// +----------------------------------------------------------------------
// | Created by [ PhpStorm ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 上海到啦网络科技有限公司.
// +----------------------------------------------------------------------
// | Create Time ( 2017/8/22 0022 - 下午 3:22)
// +----------------------------------------------------------------------
// | Author: tangyijun <251784425@qq.com>
// +----------------------------------------------------------------------
namespace app\model;

use app\extra\Sql;

class Role{
    const TABLE_NAME = 'auth_group';
    public static function _add($post = []){
        return Sql::_save(self::TABLE_NAME,$post);
    }

    public static function _update($post = []){
        return Sql::_updateByKey(self::TABLE_NAME,$post);
    }
}