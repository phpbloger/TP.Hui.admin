<?php
// +----------------------------------------------------------------------
// | Created by [ PhpStorm ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 上海到啦网络科技有限公司.
// +----------------------------------------------------------------------
// | Create Time ( 2017/8/21 0021 - 上午 9:53)
// +----------------------------------------------------------------------
// | Author: tangyijun <251784425@qq.com>
// +----------------------------------------------------------------------
namespace app\model;
use app\extra\Sql;
use think\Cookie;

class Logon{
    public static function _login(){
        $post = request()->post();
        //验证账户是否存在
        $admin_info = Sql::_find('admin',['username'=>$post['username']],'id,username,password,status');
        if(!$admin_info){
            return ['errorMsg'=>'该账户不存在'];
        }
        //验证密码是否正确
        if($admin_info['status'] == Sql::STATUS_OFF){
            return ['errorMsg'=>'账户已被锁定'];
        }
        //密码是否正确
        $check_password = password_verify($post['password'],$admin_info['password']);
        if(!$check_password){
            return ['errorMsg'=>'密码输入不正确'];
        }
        if(!empty($post['remember_me'])){
            $expire = 3600*24*7;
        }else{
            $expire = 60*30;
        }
        Cookie::set('admin_id',$admin_info['id'],['prefix'=>'admin_','expire'=>$expire]); /*存储用户信息*/
        return true;
    }
}